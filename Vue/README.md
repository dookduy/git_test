# vuedata

## Project setup
```
npm install -g @vue/cli
npm install axios
```

### Compiles and minifies for production
```
vue create hello-world
```

### Run your tests
```
npm run serve
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
